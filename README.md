微信常用API
===============

> 运行环境要求PHP7.1+。

## 安装

~~~
composer require zjb/weixin
~~~


## 版权信息

版权所有Copyright © 2020 by [zjb](http://www.zhangjingbo.top) All rights reserved。

## Developer

[ZJB](https://www.zhangjingbo.top)、DHC
