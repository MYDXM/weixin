<?php

namespace Zjb\Weixin;

use Zjb\Weixin\ErrorCode;

/**
 * XcPrpcrypt class
 * 小程序解密
 *
 */
class XcPrpcrypt
{
    public $key;

    public function __construct($k)
    {
        $this->key = $k;
    }

    /**
     * 对密文进行解密
     * @param string $aesCipher 需要解密的密文
     * @param string $aesIV 解密的初始向量
     * @return string 解密得到的明文
     */
    public function decrypt($aesCipher, $aesIV)
    {
        $decrypted = openssl_decrypt($aesCipher, 'AES-128-CBC', $this->key, OPENSSL_RAW_DATA, $aesIV);
        if ($decrypted === false) return [ErrorCode::$IllegalBuffer, null];
        try {
            //去除补位字符
            $result      = $this->decode($decrypted);
        } catch (\Exception $e) {
            //print $e;
            return [ErrorCode::$IllegalBuffer, null];
        }
        return [0, $result];
    }

    /**
     * 对解密后的明文进行补位删除
     * @param decrypted 解密后的明文
     * @return 删除填充补位后的明文
     */
    function decode($text)
    {

        $pad = ord(substr($text, -1));
        if ($pad < 1 || $pad > 32) {
            $pad = 0;
        }
        return substr($text, 0, (strlen($text) - $pad));
    }
}
