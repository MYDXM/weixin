<?php

namespace Zjb\Weixin;

use Illuminate\Support\Facades\Cache;
use Zjb\Weixin\Helper;
use Zjb\Weixin\WXBizDataCrypt;

/**
 * Class Weixin
 * 微信
 *
 * @package  App\Common\Utils
 * @version  1.0
 * @author   zjb <zjb11@foxmail.com>
 * @license  PHP Version 7.x.x {@link http://www.php.net/license/3_0.txt}
 */
class Weixin
{

    /**
     * 获取access_token
     *
     * @param int $type 类型：1公众号、2小程序、3企业
     * @return
     */
    public static function getToken(int $type = 1): string
    {
        $weixin = config('weixin') ?? Helper::getConfig();
        switch ($type) {
            case 1:
                $appId = $weixin['gzAppId'];
                $appSecret = $weixin['gzAppSecret'];
                break;
            case 2:
                $appId = $weixin['xcAppId'];
                $appSecret = $weixin['xcAppSecret'];
                break;
            case 3:
                $appId = $weixin['qyAppId'];
                $appSecret = $weixin['qyAppSecret'];
                break;
        }
        $key = "accessToken:$type>>" . substr(md5($appId . $appSecret), 0, 10);
        if (Cache::has($key)) {
            return Cache::get($key);
        }
        if (3 == $type) {
            $apiurl = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=$appId&corpsecret=$appSecret";
        } else {
            $apiurl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$appId&secret=$appSecret";
        }
        $res = Helper::geturl($apiurl);
        (!isset($res['errcode']) || 0 == $res['errcode']) && Cache::put($key, $res['access_token'], $res['expires_in'] - 100);

        return $res['access_token'] ?? $res['errmsg'];
    }


    /**
     * 获取jsConfig配置参数
     *
     * @param int $type 类型：1企业、2应用
     * @return
     */
    public static function getConfig(int $type = 1): array
    {
        $weixin = config('weixin') ?? Helper::getConfig();
        $appId = $weixin['qyAppId'];
        $appSecret = $weixin['qyAppSecret'];
        $url = $weixin['qyUrl'];
        $key = "jsapiTicket:$type>>" . substr(md5($appId . $appSecret), 0, 10);
        if (Cache::has($key)) {
            $ticket =  Cache::get($key);
        } else {
            $access_token = self::getToken(3);
            switch ($type) {
                case 1:
                    $apiurl = "https://qyapi.weixin.qq.com/cgi-bin/get_jsapi_ticket?access_token=$access_token";
                    break;
                case 2:
                    $apiurl = "https://qyapi.weixin.qq.com/cgi-bin/ticket/get?access_token=$access_token&type=agent_config";
                    break;
            }
            $res = Helper::geturl($apiurl);
            0 == $res['errcode'] && Cache::put($key, $res['ticket'], $res['expires_in'] - 100);
            $ticket = $res['ticket'] ?? '';
        }
        $noncestr = Helper::generateRandom(10);
        $time = time();
        $signature = sha1("jsapi_ticket=$ticket&noncestr=$noncestr&timestamp=$time&url=$url");
        $data = [
            'appId' => $appId,
            'timestamp' => $time,
            'nonceStr' => $noncestr,
            'signature' => $signature
        ];
        2 == $type && $data['qyAgentId'] = $weixin['qyAgentId'];

        return $data;
    }

    /**
     * 发送企业微信应用消息
     *
     * @param array $data
     * @return mixed
     */
    public static function qyAppMsg(array $data): int
    {
        $weixin = config('weixin') ?? Helper::getConfig();
        $access_token = self::getToken(3);
        $url = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=$access_token";
        $res = Helper::geturl($url, array_merge([
            'touser' => $data['touser'],
            'msgtype' => $data['msgtype'],
            'agentid' => $weixin['qyAgentId'],
            'enable_duplicate_check' => 0,
            'duplicate_check_interval' => 1800
        ], $data['msg']), 'POST', [], 1);

        return $res['errcode'] ?? 1;
    }

    /**
     * 通过code获取openid和用户信息
     *
     * @param string $code
     * @param int $is_user 1获取用户信息 0否
     * @return mixed
     */
    public static function getOpenid(string $code, int $type = 0): array
    {
        $weixin = config('weixin') ?? Helper::getConfig();
        $res = Helper::geturl("https://api.weixin.qq.com/sns/oauth2/access_token?appid={$weixin['gzAppId']}&secret={$weixin['gzAppSecret']}&code=$code&grant_type=authorization_code");
        if (1 == $type && !empty($res['access_token'])) {
            $user_info = Helper::geturl("https://api.weixin.qq.com/sns/userinfo?access_token={$res['access_token']}&openid={$res['openid']}&lang=zh_CN");
            return $user_info;
        }
        return ['openid' => $res['openid'] ?? ''];
    }

    /**
     * 公众号通过access_tokene获取用户信息
     *
     * @param array $user_list
     * @return mixed
     */
    public static function getUserInfo(array $user_list): array
    {
        $access_token = self::getToken(1);
        $res =  Helper::geturl("https://api.weixin.qq.com/cgi-bin/user/info/batchget?access_token=$access_token", ['user_list' => $user_list], 'POST', [], 1);
        return $res['user_info_list'] ?? [];
    }

    /**
     * 小程序登录效验
     *
     * @param string $code 用户登录凭证（有效期五分钟）
     * @param string $encrypted_data 用户加密数据
     * @param string $user_list 加密算法的初始向量
     * @return mixed
     */
    public static function AppletAuth(string $code, string $encrypted_data, string $iv): array
    {
        $weixin = config('weixin') ?? Helper::getConfig();
        $res = Helper::geturl("https://api.weixin.qq.com/sns/jscode2session?appid={$weixin['xcAppId']}&secret={$weixin['xcAppSecret']}&js_code=$code&grant_type=authorization_code");
        if (!empty($res['errcode'])) {
            return [];
        }
        $openid = $res['openid'] ?? '';
        $session_key = $res['session_key'];
        $errCode = (new WXBizDataCrypt($weixin['xcAppId'], $session_key))->decryptData($encrypted_data, $iv, $wxUserData, 2);
        if (0 == $errCode) {
            return $wxUserData;
        }
        return ['openId' => $openid];
    }
}
