<?php

namespace Zjb\Weixin;

/**
 * 辅助方法类
 * Class Helper
 *
 * @package App\Common\Common\Utils
 */
class Helper
{

    /**
     * curl提交
     *
     * @param string $url 地址
     * @param array $data 参数
     * @param string $type 请求方式
     * @param array $header 头部参数
     * @param int $data_type 数据类型：0数组、1json
     * @return
     */
    public static function geturl(string $url, array $data = [], $type = 'GET', array $header = [], int $data_type = 0, int $is_json = 1)
    {
        if (1 == $data_type) {
            $data = json_encode($data);
            $header = array_merge($header, ['Content-Type: application/json', 'Content-Length: ' . strlen($data)]);
        }
        //初使化init方法
        $ch = curl_init();
        //指定URL
        curl_setopt($ch, CURLOPT_URL, $url);
        //设定请求后返回结果
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //声明使用$type方式来进行发送
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        //头部参数
        if (!empty($header)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }
        //发送数据
        if (!empty($data)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        //忽略证书
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        //忽略header头信息
        curl_setopt($ch, CURLOPT_HEADER, 0);
        //设置超时时间
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        //发送请求
        $output = curl_exec($ch);
        //关闭curl
        curl_close($ch);
        //file_put_contents('curl.txt', print_r([$data,$header], true));
        //返回数据
        return $is_json ? json_decode($output, true) : $output;
    }

    /**
     * 生成随机字符串
     *
     * @param int $len
     * @param int $type (1=>数字 , 2=>字母 , 3=>混合)
     *
     * @return bool|string
     */
    public static function generateRandom($len = 32, $type = 3)
    {
        $arr[1] = [1, 2, 3, 4, 5, 6, 7, 8, 9];
        $arr[2] = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'z'];
        $arr[3] = ['b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'z', '2', '3', '4', '5', '6', '7', '8', '9'];
        $word = '';
        $cnt = count($arr[$type]) - 1;
        mt_srand((float)microtime() * 1000000);
        shuffle($arr[$type]);
        for ($i = 0; $i < $len; $i++) {
            $word .= $arr[$type][random_int(0, $cnt)];
        }
        if (strlen($word) > $len) {
            $word = substr($word, 0, $len);
        }
        return $word;
    }

    /**
     * 获取配置
     *
     * @return array
     */
    public static function getConfig()
    {
        return require(dirname(__FILE__).'/../config/weixin.php');
    }
}
