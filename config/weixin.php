<?php
return [
    'gzAppId' => env('gzAppId' ?? ''),
    'gzAppSecret' => env('gzAppSecret' ?? ''),
    'xcAppId' => env('xcAppId' ?? ''),
    'xcAppSecret' => env('xcAppSecret' ?? ''),
    'qyAppId' => env('qyAppId' ?? ''),
    'qyAppSecret' => env('qyAppSecret' ?? ''),
    'qyUrl' => env('qyUrl' ?? ''),
    'qyAgentId' => env('qyAgentId' ?? ''),
    'qyToken' => env('qyToken' ?? ''),
    'qyEncodingAESKey' => env('qyEncodingAESKey' ?? ''),
];
